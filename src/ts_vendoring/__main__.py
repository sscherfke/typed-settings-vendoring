from pathlib import Path

import attrs

from ._vendor import typed_settings as ts


@attrs.frozen
class Settings:
    spam: str = "spam"
    eggs: Path = Path("pyproject.toml")


@ts.cli(Settings, "ts_vendoring")
def main(settings: Settings):
    """
    Run example application
    """
    print("Hello, world!")
    print("Loaded settings:", settings)


if __name__ == "__main__":
    main()
