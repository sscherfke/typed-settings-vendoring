import pytest

import ts_vendoring.__main__


def test_main(capsys: pytest.CaptureFixture) -> None:
    ts_vendoring.__main__.main()
    out, err = capsys.readouterr()
    assert out == (
        "Hello, world!\n"
        "Loaded settings: Settings(spam='spam', eggs=PosixPath('pyproject.toml'))\n"
    )
    assert err == ""
