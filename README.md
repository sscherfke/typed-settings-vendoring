# Typed Settings Vendoring

This is an example application that demonstrates how you can vendor [Typed Settings](https://gitlab.com/sscherfke/typed-settings).
This allows your app to work with minimal (or no) dependencies.


## How it works

- You create a normal application.
- Vendored packages live in `src/{your_package}/_vendored`.
- Packages to be vendored are defined in `src/{your_package}/_vendored/vendored.txt` (format like pip requirement files).
- You can update/install vendored packages with `python tools/vendored.py`.
- You can import vendored packages with `from [your_package]._vendored import {vendored_pkg}`.
