import pathlib
import re
import shutil
import subprocess
import sys


PKG_NAME = "ts_vendoring"
VENDOR_DIR = pathlib.Path(__file__).parent.parent.joinpath("src", PKG_NAME, "_vendor")


def update_vendored() -> None:
    install(VENDOR_DIR)
    new_root = f"{PKG_NAME}._vendor"
    rewrite_attrs(VENDOR_DIR.joinpath("attrs"), new_root)
    rewrite_cattrs(VENDOR_DIR.joinpath("cattrs"), new_root)
    rewrite_typed_settings(VENDOR_DIR.joinpath("typed_settings"), new_root)


def rewrite_attrs(pkg_files: pathlib.Path, new_root: str) -> None:
    """
    Rewrite imports in packaging to redirect to vendored copies.
    """
    for file in pkg_files.rglob("*.py"):
        text = file.read_text()
        text = re.sub(r"from (attr[ .])", rf"from {new_root}.\1", text)
        text = text.replace("import attr", f"from {new_root} import attr")
        file.write_text(text)


def rewrite_cattrs(pkg_files: pathlib.Path, new_root: str) -> None:
    """
    Rewrite imports in packaging to redirect to vendored copies.
    """
    shutil.rmtree(pkg_files.parent.joinpath("cattr"))
    shutil.rmtree(pkg_files.parent.joinpath("cattrs", "preconf"))
    for file in pkg_files.rglob("*.py"):
        text = file.read_text()
        text = re.sub(r"from (cattrs[ .])", rf"from {new_root}.\1", text)
        text = text.replace("from attr import", f"from {new_root}.attr import")
        text = text.replace("import attr", f"from {new_root} import attr")
        text = text.replace(
            "from exceptiongroup import", f"from {new_root}.exceptiongroup import"
        )
        file.write_text(text)


def rewrite_typed_settings(pkg_files: pathlib.Path, new_root: str) -> None:
    """
    Rewrite imports in jaraco.read_text to redirect to vendored copies.
    """
    for file in pkg_files.glob("*.py"):
        text = file.read_text()
        text = re.sub(r"from (cattrs[ .])", rf"from {new_root}.\1", text)
        text = re.sub(r"from (attrs?[ .])", rf"from {new_root}.\1", text)
        text = re.sub(r"import ((c?)attr(s?))", rf"from {new_root} import \1", text)
        file.write_text(text)


def install(vendor: pathlib.Path) -> None:
    clean(vendor)
    install_args = [
        sys.executable,
        *("-m", "pip"),
        "install",
        *("-r", str(vendor.joinpath("vendored.txt"))),
        *("-t", str(vendor)),
    ]
    subprocess.check_call(install_args)
    vendor.joinpath("__init__.py").write_text("")


def clean(vendor: pathlib.Path) -> None:
    """
    Remove all files out of the vendor directory except the meta
    data (as pip uninstall doesn't support -t).
    """
    paths = (path for path in vendor.glob("*") if path.name != "vendored.txt")
    for path in paths:
        shutil.rmtree(path) if path.is_dir() else path.unlink()


if __name__ == "__main__":
    update_vendored()
